﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task29
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Count the number of words in a string
                The quick brown fox jumped over the fence
            */

            // Initialise variables
            var sentence = "The quick brown fox jumped over the fence";
            // Split sentence into array by ' ' 
            string[] words = sentence.Split(' ');

            // Print resulting message
            Console.WriteLine($"There are {words.Count()} words in the sentence: {sentence}.");
        }
    }
}
