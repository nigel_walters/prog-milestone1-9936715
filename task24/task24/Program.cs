﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task24
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Create a program that shows a menu where there user makes a selection
                User makes a selection
                Screen clears
                User presses m or M and is taken back to the main menu
            */

            // Initialise variables
            var select = 0;
            var menu = "";
            var check = true;


            // Show main menu and prompt for user for selection
            do
            {
                Console.WriteLine(@"
Main menu
---------
Enter a number:
1. Option A
2. Option B
3. Option C
4. Option D
5. Option E
6. Exit
");
                // Get user selection
                check = int.TryParse(Console.ReadLine(), out select);

                // Check if it is an option
                if (select > 0 && select < 6)
                {
                    Console.Clear();
                    do
                    {
                        Console.Write("Press m or M to return to the main menu: ");
                        menu = Console.ReadLine();
                    } while (menu != "m" && menu != "M");
                }
                Console.Clear();
            } while (select != 6);
        }
    }
}
