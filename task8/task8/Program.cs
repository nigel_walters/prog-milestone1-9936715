﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task8
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Forloop - print a countdown from 99 to 0 using no more than 5 lines of code in the loop block
            */

            // Print countdown from 99
            for(int i=99; i>=0; i--)
            {
                Console.WriteLine(i);
            }
        }
    }
}
