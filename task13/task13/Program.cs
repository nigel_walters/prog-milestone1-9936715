﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task13
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                This tasks requires input from user for 3 doubles and prints result to the screen
                Mini shop - the sum of  3 doubles by user input and add GST (15%) 
                Output must show $ signs
            */

            // Initialise variables
            double[] amount = new double[3];
            var total = 0.0;
            var check = true;

            // Get user input
            Console.WriteLine("Calculate total GST");
            Console.WriteLine("Enter 3 decimal numbers:");
            for (int i = 0; i < 3; i++)
            {
                do
                {
                    Console.Write($"{i + 1}: ");
                    check = double.TryParse(Console.ReadLine(), out amount[i]);
                } while (!check);

                // Add amount to total
                total += amount[i];
            }

            // Calculate total including GST
            total = total / 0.85;

            // Display total inclusive
            Console.WriteLine("Total including GST\n" + total.ToString("C"));
        }
    }
}
