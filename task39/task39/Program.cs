﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task39
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Assuming that the 1st of the month is on a Monday (like this month) write a program that tells you how many mondays are in a month.
            */

            // Get number of days in the month of August 2016
            var daysInMonth = DateTime.DaysInMonth(2016, 8);
            // Counter for number of Mondays
            var numberOfMondays = 0;

            // Iterate through loop to days in the month
            for (int i = 0; i < daysInMonth; i++)
            {
                // Determine the day, Monday is 0
                var mod = i % 7;
                // If Monday, increment counter
                if (mod == 0)
                    numberOfMondays++;
            }

            // Display the number of Mondays in the month
            Console.WriteLine($"There are {numberOfMondays} Mondays in the month of August 2016");
        }
    }
}
