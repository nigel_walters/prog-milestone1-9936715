﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task33
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Write a program that will tell you how many tutorial groups need to be made based on a number given by the user. Each tutorial group has a maximum of 28 students.
            */

            // Initalise variables
            var number = 0;
            var groups = 0;
            const int size = 28;
            var isNumber = true;

            Console.WriteLine("Tutorial Groups");

            // Get number
            do
            {
                Console.Write("Enter number of people: ");
                isNumber = int.TryParse(Console.ReadLine(), out number);
            } while (!isNumber);
            // Calculate number of groups
            groups = number / size;

            if (number % size != 0)
                groups++;

            // Display message indicating number of groups required
            Console.WriteLine($"You will need {groups} tutorial group{(groups == 1 ? "" : "s")} for {number} {(number == 1 ? "person" : "people")}");
        }
    }
}
