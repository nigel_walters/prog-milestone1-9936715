﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task6
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Forloop - calculate the sum of 5 numbers to n (n given to you by user)
            */

            // Initialise and declare variables
            var sum = 0;
            int[] numbers = new int[5];
            var check = true;

            // Prompt for user input
            Console.WriteLine("Please enter 5 numbers:");

            // Loop user input into array and add to sum
            for(int i =0; i<5; i++)
            {
                do
                {
                    Console.Write($"{i + 1}: ");
                    check = int.TryParse(Console.ReadLine(), out numbers[i]);
                } while (!check);
                sum += numbers[i];
            }

            // Display sum of numbers
            Console.WriteLine($"Sum: {sum}");
        }
    }
}
