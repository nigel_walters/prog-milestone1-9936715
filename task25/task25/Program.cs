﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task25
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Write a program that takes user input for a number and cannot crash
                Check what happens when you get a word
                Check what happens when you get a number
            */

            // Intialise variables
            var number = 0;
            var isNumber = true;

            // Get number and re-prompt if not an integer
            do
            {
                Console.Write("Enter a number: ");
                isNumber = int.TryParse(Console.ReadLine(), out number);
                if (!isNumber)
                {
                    Console.WriteLine("Invalid input.");
                }
            } while (!isNumber);

            // Print number
            Console.WriteLine($"You entered: {number}");
        }
    }
}
