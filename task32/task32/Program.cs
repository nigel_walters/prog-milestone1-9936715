﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task32
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Write a program that says what number each day of the week is - you must use an array
            */

            // Initialise array with days
            string[] days = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

            // Iterate through array and display day number to name
            for (int i = 0; i < 7; i++)
            {
                Console.WriteLine($"Day {i + 1}: {days[i]}");
            }
        }
    }
}
