﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Time from 24hr to pm / am (show what am / pm time is when the number is between 12 and 24) (Only worry about the input for the hour - don’t worry about the minutes)
            */

            // Initialise variables
            var hour = 0;
            var time = "";
            var check = true;

            // Accept numerical input from 0 - 23
            do
            {
                Console.Write("Please enter a value from 0 - 23: ");
                check = int.TryParse(Console.ReadLine(), out hour);
            } while (hour < 0 || hour > 23 || !check);

            // Assign time based on input
            time = new DateTime(1, 1, 1, hour, 0, 0, 0).ToShortTimeString();

            //Display time
            Console.WriteLine($"{time}");
        }
    }
}
