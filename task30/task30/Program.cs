﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task30
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Take in 2 number from a user and show the difference when they are added as a string and as a number
            */

            // Initialise input variables
            int[] number = new int[2];
            var isNumber = true;

            // Get numbers
            Console.WriteLine("Enter 2 numbers");
            for (int i = 0; i < 2; i++)
            {
                do
                {
                    Console.Write($"{i + 1}: ");
                    isNumber = int.TryParse(Console.ReadLine(), out number[i]);
                } while (!isNumber);
            }

            // Display addition of numbers
            Console.WriteLine($"Add as strings: {number[0].ToString() + number[1].ToString()}");
            Console.WriteLine($"Add as numbers: {number[0] + number[1]}");
        }
    }
}
