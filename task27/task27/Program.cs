﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task27
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Create an array with the following values and print it in ascending order to the screen.
                Red, Blue, Yellow, Green, Pink
            */

            // Initialise array
            string[] colours = { "Red", "Blue", "Yellow", "Green", "Pink" };

            // Sort array alphabetically
            Array.Sort(colours);

            // Display in ascending order
            foreach (string colour in colours)
            {
                Console.WriteLine(colour);
            }

        }
    }
}
