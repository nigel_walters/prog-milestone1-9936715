﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Get user input for name and age and show 3 different ways to print a string
            */


            // Initialise variables
            var name = "";
            var age = 0;
            var check = true;
            var plural = "";

            // Get user input for name and age
            Console.Write("Enter your name: ");
            name = Console.ReadLine();
            do
            {
                Console.Write("Enter your age: ");
                check = int.TryParse(Console.ReadLine(), out age);
            } while (!check);

            plural = (age == 1 ? "year old" : "years old");

            // Print string 3 different ways
            Console.WriteLine("Hi " + name + ", you are " + age + " " +  plural);
            Console.WriteLine("Hi {0}, you are {1} {2}", name, age, plural);
            Console.WriteLine($"Hi {name}, you are {age} {plural}");
        }
    }
}
