﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task37
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Write a program that calculates how many hours you should study per week for a 15 credit paper.
                Use the following data to create your app.
                    There are 10 hours of study for every credit of the paper
                    Each week gives 5 contact hours (hours in lecture and tutorials)
                    The Semester is 12 weeks long.
            */

            // Initialise variables
            var hoursPerCredit = 10;
            var numberOfCredits = 15;
            var weeklyContactHours = 5;
            var weeksPerSemester = 12.0;
            var weeklyStudyHours = 0.0;

            // Calculate the number of hours to study per week
            weeklyStudyHours = (hoursPerCredit * numberOfCredits) / weeksPerSemester + weeklyContactHours;

            // Diplay message
            Console.WriteLine($"{weeklyStudyHours} hours is the required amount of study per week for a {numberOfCredits} credit paper");
        }
    }
}
