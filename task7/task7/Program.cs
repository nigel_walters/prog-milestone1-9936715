﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task7
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Forloop - show times table from 1 x to 12 x for a given number - each answer on a new line
            */

            // Initialise factor
            var factor = 0;
            var check = true;

            // Get factor
            Console.WriteLine("Timestable");
            do {
                Console.Write("Enter a number: ");
                check = int.TryParse(Console.ReadLine(), out factor);
            } while(!check);

            // Print timestable
            for (int i = 0; i < 12; i++)
            {
                var number = i + 1;
                Console.WriteLine($"{factor} x {number} = {factor * number}");
            }
        }
    }
}
