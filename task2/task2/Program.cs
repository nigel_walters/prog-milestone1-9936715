﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Get user input for the month and day they are born on and display it in a sentence

            // Initialise variables
            var month = 0;
            var day = 0;
            var mod = 0;
            var _month = "";
            var _day = "";
            var __day = true;
            var __month = true;

            // Prompt and assign input variables
            Console.WriteLine("When is your birthday?");

            // Accept numerical input from 1 - 31 (maximum days in a month)
            do
            {
                Console.Write("Enter the day number: ");
                __day = int.TryParse(Console.ReadLine(), out day);
            }
            while (day < 1 || day > 31 || !__day);

            // Accept numerical input from 1 - 12 (months in a year)
            do
            {
                Console.Write("Enter the month number: ");
                __month = int.TryParse(Console.ReadLine(), out month);
            }
            while (month < 1 || month > 12 || !__month);
            
            // Assign month name
            _month = new DateTime(1, month, 1).ToString("MMMM");

            // Assign appropriate day postfix
            mod = day % 10;
            if (mod == 1 && !(day == 11))
            {
                _day = $"{day}st";
            }
            else if (mod == 2 && !(day == 12))
            {
                _day = $"{day}nd";
            }
            else if (mod == 3 && !(day == 13))
            {
                _day = $"{day}rd";
            }
            else
            {
                _day = $"{day}th";
            }

            // Display Message of birthday
            Console.WriteLine($"\nYour birthday is on the {_day} of {_month}!");
        }
    }
}
