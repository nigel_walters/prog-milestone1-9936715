﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task10
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Write a program that tells you how many leap years are there in the next 20 years (with calculation)
            */

            // Initialise variables
            var year = DateTime.Now.Year;
            var leap = 0;

            // Calculate number of leap years
            for(int i = 0; i<20; i++)
            {
                var mod = year % 4;
                if(mod == 0)
                {
                    leap++;
                }
                year++;
            }

            // Print number of leap years
            Console.WriteLine($"There are {leap} leap years in the next 20 years");
        }
    }
}
