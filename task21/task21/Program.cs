﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task21
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Task requires checking the values of a Key and print it if it is true
                Create a Dictionary and populate it with strings and int
                Fill it up with months and days of a year
                Console.WriteLine - Display all the months that have 31 days 
            */

            // Initialise variables
            Dictionary<string, int> d = new Dictionary<string, int>();
            var year = DateTime.Now.Year;

            // Add values to dictionary
            for (int i = 1; i <= 12; i++)
            {
                var days = DateTime.DaysInMonth(year, i);
                var month = new DateTime(year, i, days).ToString("MMMM");
                d.Add(month, days);
            }

            // Display months that have 31 days
            Console.WriteLine($"Months of {year} that have 31 days:");
            foreach (var x in d)
            {
                if (x.Value == 31)
                    Console.WriteLine(x.Key);
            }
        }
    }
}
