﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task23
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Task requires checking the values of a Key and print it if it is true
                Create a dictionary filled with days of the week and weekends
                Display to the user which days are week days and which are weekends.
            */

            // Declare dictionary
            Dictionary<string, int> d = new Dictionary<string, int>();

            // Set start date to Monday
            DateTime dt = new DateTime(2016, 8, 1);

            // Add days to dictionary
            for (int i = 1; i <= 7; i++)
            {
                d.Add(dt.ToString("dddd"), i);
                dt = dt.AddDays(1);
            }

            // Iterate through dictionary and list week days
            Console.WriteLine("Weekdays:");
            foreach (var x in d)
            {
                if (x.Value <= 5)
                    Console.WriteLine($"\t{x.Key}");
            }

            // Iterate through dictionary and list weekend days
            Console.WriteLine("Weekend:");
            foreach (var x in d)
            {
                if (x.Value > 5)
                    Console.WriteLine($"\t{x.Key}");
            }
        }
    }
}
